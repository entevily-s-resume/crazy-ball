﻿using System;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public Player greenPlayer;
    public Player bluePlayer;

    public Color defaultColor;
    public Color blueColor;
    public Color greenColor;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            Instance = this;
        }
    }
    public enum IODevice
    {
        MOUSE,
        KEYBAORD
    }

    public IODevice ioDevice = IODevice.KEYBAORD;

    public int vollyCount = 1;

    internal void TakeDamage(PaddleController.PLAYERCOLOR wallColor, float damage)
    {
        if(wallColor == PaddleController.PLAYERCOLOR.GREEN)
        {
            greenPlayer.TakeDamage(damage);
            UIController.Instance.UpdateHealth(wallColor);
        }
        else
        {
            bluePlayer.TakeDamage(damage);
            UIController.Instance.UpdateHealth(wallColor);
        }
    }

    private void Update()
    {
        if(greenPlayer.health <= 0)
        {
            UIController.Instance.ShowWinner(PaddleController.PLAYERCOLOR.GREEN);
            
        }else if(bluePlayer.health <= 0)
        {
            UIController.Instance.ShowWinner(PaddleController.PLAYERCOLOR.BLUE);
        }
    }

    internal void StartGame()
    {
        vollyCount = 1;
        bluePlayer.health = 100;
        greenPlayer.health = 100; 
   
        UIController.Instance.UpdateHealth(PaddleController.PLAYERCOLOR.BLUE);
        UIController.Instance.UpdateHealth(PaddleController.PLAYERCOLOR.GREEN);
        MoveBall.Instance.StartGame();
    }

    internal void StopGame()
    {
        MoveBall.Instance.StopGame();
    }
}
