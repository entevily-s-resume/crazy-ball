﻿using System;

[Serializable]
public class Player
{
    public float health = 100;
    public float maxHealth = 100;

    public void TakeDamage(float damage)
    {
        health-=damage;

        if (health <=0)
            health = 0;
    }
}