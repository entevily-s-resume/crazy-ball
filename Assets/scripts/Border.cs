﻿using UnityEngine;

public class Border : MonoBehaviour
{

    public enum POS {
        TOP, 
        BOTTOM,
        RIGHT,
        LEFT
    }

    public POS position;
    public PaddleController.PLAYERCOLOR wallColor;
}
